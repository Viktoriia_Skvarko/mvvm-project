//
//  CellViewModelTypeProtocol.swift
//  MVVMProject
//
//  Created by Viktoriia Skvarko on 11.08.2021.
//

import Foundation

protocol CellViewModelTypeProtocol: AnyObject {
    var brandAndmodel: String { get }
    var yearProdaction: String { get }
}
