//
//  CarTableViewModelTypeProtocol.swift
//  MVVMProject
//
//  Created by Viktoriia Skvarko on 11.08.2021.
//

import Foundation

protocol CarTableViewModelTypeProtocol {
    func numberOfRows() -> Int
    func cellCarViewModel(forIndexPath indexPath: IndexPath) -> CellViewModelTypeProtocol?
}
