//
//  CarModel.swift
//  MVVMProject
//
//  Created by Viktoriia Skvarko on 11.08.2021.
//

import Foundation

struct CarModel {
    var brandCar: String
    var modelCar: String
    var yearProdactionCar: Int
}
