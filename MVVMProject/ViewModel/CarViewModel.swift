//
//  CarViewModel.swift
//  MVVMProject
//
//  Created by Viktoriia Skvarko on 11.08.2021.
//

import Foundation

class CarViewModel: CarTableViewModelTypeProtocol {
    
    var cars = [
        CarModel(brandCar: "Mercedes-Benz", modelCar: "W245", yearProdactionCar: 2005),
        CarModel(brandCar: "Audi", modelCar: "A8", yearProdactionCar: 2011),
        CarModel(brandCar: "Toyota", modelCar: "RAV4", yearProdactionCar: 2010),
        CarModel(brandCar: "Honda", modelCar: "CR-V", yearProdactionCar: 2014),
        CarModel(brandCar: "Ford", modelCar: "Focus", yearProdactionCar: 2012)
    ]
    
    func numberOfRows() -> Int {
        return cars.count
    }
    
    func cellCarViewModel(forIndexPath indexPath: IndexPath) -> CellViewModelTypeProtocol? {
        let car = cars[indexPath.row]
        return CarCellViewModel(car: car)
    }
}
