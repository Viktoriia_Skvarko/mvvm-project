//
//  CellCarViewModel.swift
//  MVVMProject
//
//  Created by Viktoriia Skvarko on 11.08.2021.
//

import Foundation

class CarCellViewModel: CellViewModelTypeProtocol {
    
    // MARK: - Private properties
    
    private var car: CarModel
    
    
    // MARK: - Bisiness logic
    
    var brandAndmodel: String {
        return car.brandCar + " " + car.modelCar
    }
    
    var yearProdaction: String {
        return String(describing: car.yearProdactionCar)
    }
    
    init(car: CarModel) {
        self.car = car
    }
}
