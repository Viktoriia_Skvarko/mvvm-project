//
//  CarTableViewCell.swift
//  MVVMProject
//
//  Created by Viktoriia Skvarko on 11.08.2021.
//

import UIKit

class CarTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var brandAndModel: UILabel!
    @IBOutlet weak var yearProdaction: UILabel!
    
    
    // MARK: - Bisiness logic
    
    weak var carViewModel: CellViewModelTypeProtocol? {
        willSet(carViewModel) {
            guard let carViewModel = carViewModel else { return }
            brandAndModel.text = carViewModel.brandAndmodel
            yearProdaction.text = carViewModel.yearProdaction
        }
    }
}
