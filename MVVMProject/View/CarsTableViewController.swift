//
//  CarsTableViewController.swift
//  MVVMProject
//
//  Created by Viktoriia Skvarko on 11.08.2021.
//

import UIKit

class CarsTableViewController: UITableViewController {
    
    // MARK: - Public Properties
    
    var carViewModel: CarTableViewModelTypeProtocol?
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        carViewModel = CarViewModel()
        self.tableView.reloadData()
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carViewModel?.numberOfRows() ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarCell", for: indexPath) as? CarTableViewCell
        
        guard let tableViewCell = cell, let carViewModel = carViewModel else { return UITableViewCell() }
        
        let cellViewModel = carViewModel.cellCarViewModel(forIndexPath: indexPath)
        tableViewCell.carViewModel = cellViewModel
        return tableViewCell
    }
}
